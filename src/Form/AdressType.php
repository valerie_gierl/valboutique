<?php

namespace App\Form;

use App\Entity\Adress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label'=>'Quel nom souhaitez-vous donner à cet adresse? ',
                'attr'=>[
                    'placeholder'=>'Nommez votre adresse '
                ]
            ])
            ->add('firstname', TextType::class,[
                'label'=>'A quel prénom ? ',
                'attr'=>[
                    'placeholder'=>'Indiquez le prénom '
                ]
            ])
            ->add('lastname', TextType::class,[
                'label'=>'A quel nom? ',
                'attr'=>[
                    'placeholder'=>'Indiquezle nom '
                ]
            ])
            ->add('company', TextType::class,[
                'label'=>'Nom de la société',
                 'required'=>false,
                'attr'=>[
                    'placeholder'=>'(facultatif) Entrez le nom de votre société ',
                   
                ]
            ])
            ->add('adress', TextType::class,[
                'label'=>'Votre adresse ? ',
                'attr'=>[
                    'placeholder'=>'Numéro, Nom de la rue '
                ]
            ])
            ->add('postal', TextType::class,[
                'label'=>'Code postal',
                'attr'=>[
                    'placeholder'=>'Entrez le code postal'
                ]
            ])
            ->add('city', TextType::class,[
                'label'=>' ville ',
                'attr'=>[
                    'placeholder'=>'Entrez la ville '
                ]
            ])
            ->add('country', CountryType::class,[
                'label'=>'Pays',
                'attr'=>[
                    'placeholder'=>'Dans quel pays'
                ]
            ])
            ->add('phone',  TelType::class,[
                'label'=>'Téléphone',
                'attr'=>[
                    'placeholder'=>'Veuillez saisir un numéro de téléphone '
                ]
            ])
            ->add('submit',SubmitType::class,[
                'label'=>"Valider",
                'attr'=>[
                    'class'=>'btn-block btn-info'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adress::class,
        ]);
    }
}
