<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/nous-contacter", name="contact")
     */
    public function index(Request $request): Response
    {

        $form =$this->createForm(ContactType::class);
        $form->handleRequest($request);
    
        
        if($form->isSubmitted() && $form->isValid()){

            $this->addFlash('notice','Merci d\'avoir pris le temps de naviguer et de me contacter');

            $mail = new Mail();
            $content =$form->get('content')->getData(); 
            $mail->send('valerie.gierlowski@gmail.com','Valarôme Boutique','Vous avez reçu un email à partir de Valarôme Boutique/Symfony5',$content);
        }
            
            return $this->render('contact/index.html.twig', [
                'form'=> $form->createView()
            ]); 
       
    }   
  
}
