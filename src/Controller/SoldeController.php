<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SoldeController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
            $this->entityManager = $entityManager;
    }

    /**
     * @Route("/solde", name="solde")
     */
    public function index(): Response
    {
        $soldes = $this->entityManager->getRepository(Product::class)->findBySolde(1);

        return $this->render('solde/index.html.twig',[
            'soldes'=>$soldes
        ]);
    }
}
