<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class StripeController extends AbstractController
{
    /**
     * @Route("/commande/create-session/{reference}", name="stripe_create_session")
     */
    public function index(EntityManagerInterface $entityManager, Cart $cart, $reference): Response
    {
        if($reference[0]==" ")
          $reference = substr($reference,1);
        $order = $entityManager->getRepository(Order::class)->findOneByReference($reference);
        if(!$order){
          return new JsonResponse(['error'=>'order']);

        }
  
        $products_for_stripe = [];
        $YOUR_DOMAIN = 'https://valboutique.valeriegierlowski.eu/';
            

        foreach($order->getOrderDetails()->getValues() as $produit){
   $prix =0;
   if($produit->getPriceSolde()){
     $prix= $produit->getPriceSolde();
   }else{
     $prix= $produit->getPrice();
   }

     $produit_object = $entityManager->getRepository(Product::class)->findOneByName($produit->getProduct());
            $products_for_stripe[] = [ 
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $prix,
                    'product_data' => [
                      'name' => $produit->getProduct(),
                      'images' => [$YOUR_DOMAIN."/uploads/".$produit_object->getIllustration()]
                    ],
                  ],

                  'quantity' => $produit->getQuantity(),
                ];

        }

        $products_for_stripe[] = [
                  'price_data' => [
                'currency' => 'eur',
                'unit_amount' => $order->getCarrierPrice(),
                'product_data' => [
                  'name' => $order->getCarrierName()
                //   'images' => [$YOUR_DOMAIN],
                ],
              ],
              'quantity' => 1,
            ];
        

        Stripe::setApiKey('sk_test_51J3FjAE0oCx2AVXUFXA7JuTbEy04J7R1MSKWKS502fKyncochYC8IboXdsQssWB6G1R2zWhwBQ6Uy3IwBmRox6Ay00IKrFsx91');

            
        $checkout_session = Session::create([
            'customer_email'=> $this->getUser()->getEmail(),
           'payment_method_types' => ['card'],
           'line_items' => [
               $products_for_stripe
           ],
           'mode' => 'payment',
           'success_url' => $YOUR_DOMAIN . 'commande/merci/{CHECKOUT_SESSION_ID}',
           'cancel_url' => $YOUR_DOMAIN . 'commande/erreur/{CHECKOUT_SESSION_ID}',
         ]);


      
         $order->setStripeSessionId($checkout_session->id);
         $entityManager->flush();
       

        $response = new JsonResponse(['id' => $checkout_session->id]);
        return $response;
     
}
}