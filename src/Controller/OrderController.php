<?php

namespace App\Controller;


use App\Classe\Cart;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Form\OrderType;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }
    /**
     * @Route("/commande", name="order")
     */
    public function index(Cart $cart, Request $request): Response
    {

        if(!$this->getUser()->getAdresses()->getValues())
        {
            return $this->redirectToRoute('account_adress_add');
        }
        $form =$this->createForm(OrderType::class,null,[
            'user'=>$this->getUser()
        ]);

      

        return $this->render('order/index.html.twig',[
            'form'=>$form->createView(),
            'cart'=>$cart->getFull()
        ]);
    }

    
    /**
     * @Route("/commande/recapitulatif", name="order_recap", methods={"POST"})
     */
    public function add(Cart $cart, Request $request): Response
    {

        $form =$this->createForm(OrderType::class,null,[
            'user'=>$this->getUser()
        ]);

      $form->handleRequest($request);
      
      if($form->isSubmitted() && $form->isValid()){

        $user = $this->getUser();
        $date = new DateTime();  
        $carriers= $form->get('carriers')->getData();
        $delivery= $form->get('adresses')->getData();
        $delivery_content = $delivery->getFirstname().' '.$delivery->getLastname();
        $delivery_content .= '<br/>'.$delivery->getPhone();
        
        if($delivery->getCompany()){
            $delivery_content .= '<br/>'.$delivery->getCompany();

        }
        $delivery_content .= '<br/>'.$delivery->getAdress();
        $delivery_content .= '<br/>'.$delivery->getPostal().' '.$delivery->getCity();
        $delivery_content .= '<br/>'.$delivery->getCountry();


   

     //enregistrer commande
     $order = new Order();
     $reference= $date->format('dmY').'-'.uniqid();
     if($reference[0]==" ")
          $reference = substr($reference,1);
    $order->setReference($reference);

     $order->setUser($user);
    $order->setCreatedAt($date);
     $order->setCarrierName($carriers->getName());
     $order->setCarrierPrice($carriers->getPrice());
     $order->setDelivery($delivery_content);
    $order->setState(0);

   $this->entityManager->persist($order);


   //enregistrer produits commandes soit OrderDetails()
   
        foreach($cart->getFull() as $produit){
  
         
            $orderDetails = new OrderDetails();
            $orderDetails ->setmyOrder($order);
            $orderDetails->setProduct($produit['product']->getName());
            $orderDetails->setQuantity($produit['quantity']);
            //si produit est soldé
            if($produit['product']->getPriceSolde()){

                //pour se rappeler eventuellement du prix non soldé
                $orderDetails->setPrice($produit['product']->getPrice());
                $orderDetails->setPriceSolde($produit['product']->getPriceSolde());
                $orderDetails->setTotal($produit['product']->getPriceSolde() * $produit['quantity']);
                           }else {
                            $orderDetails->setPrice($produit['product']->getPrice());
                   
                            $orderDetails->setTotal($produit['product']->getPrice() * $produit['quantity']); 

                           }
        
           
            $this->entityManager->persist($orderDetails);

             
    
    
            }
        

         $this->entityManager->flush();


            return $this->render('order/add.html.twig',[

                        'cart'=>$cart->getFull(),
                        'carrier'=>$carriers,
                        'delivery'=>$delivery_content,
                        'reference'=>$order->getReference()
                     
                    ]);
 }
      
return $this->redirectToRoute('cart');


}
}