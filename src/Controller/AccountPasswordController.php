<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountPasswordController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/compte/modifier-mot-de-passe", name="account_password")
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
$notif = null;
$css=null;

        $user = $this->getUser();
        $form =$this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $old_pwd = $form->get('old_password')->getData();
  
            if($encoder->isPasswordValid($user, $old_pwd)){
                $new_psw=$form->get('new_password')->getData();
                $password = $encoder->encodePassword($user,$new_psw);

                $user->setPassword($password);

                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $notif = 'Votre changement a bien été pris en compte';
                $css= 'alert alert-success';

            } else {
                $notif = "Votre mot de passe actuel n'est pas le bon !";
                $css='alert alert-danger';
            }
        }


        return $this->render('account/password.html.twig', [
            'form' =>$form->createView(),
            'notification'=>$notif,
            'css'=>$css
        ]);
    }
}
