<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\ResetPassword;
use App\Entity\User;
use App\Form\ResetPasswordType;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager )
    {
        $this->entityManager=$entityManager;
        
    } 
    
    /**
     * @Route("/mot-de-passe-oublie", name="reset_password")
     */
    public function index(Request $request): Response
    {

        if($this->getUser()){
            return $this->redirectToRoute('home');
        }

        if($request->get('email')){

         $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->get('email'));
    
         if($user){
            //enregistre dans base la demande reset password avec user et token, createdAt
            $reset_password = new ResetPassword();
            $reset_password->setUser($user);
            $reset_password->setToken(uniqid());
            $reset_password->setCreatedAt(new DateTime());
            $this->entityManager->persist($reset_password);
            $this->entityManager->flush();
  

         //envoi d'un email à l'utilisateur


    $url = $this->generateUrl('update_password', [
        'token' => $reset_password->getToken(),
    ]);
    $content = "Bonjour "." ".$user->getFirstname(). "<br/> Vous avez souhaité réinitialiser votre mot de passe sur Valarôme Boutique<br/><br/>";
    $content .="Merci de bien vouloir cliquer sur le lien suivant <a href='".$url."'> pour mettre à jour votre mot de passe</a>.";
    
    $mail = new Mail();
    $mail->send($user->getEmail(), $user->getFirstname()." ".$user->getLastname(),'Réinitialiser votre mot de passe sur Valarôme Boutique',$content);
    $this->addFlash('notice','Un mail vient de vous être envoyé afin de réinitialiser votre mot de passe');

        } else{
            $this->addFlash('notice','Cette adresse email n\'est pas valide');
        }
       
    }  

    return $this->render('reset_password/index.html.twig');
}



    /**
     * @Route("/modifier-mot-de-passe/{token}", name="update_password")
     */
         public function update(Request $request, $token, UserPasswordEncoderInterface $encoder){

            $reset_password = $this->entityManager->getRepository(ResetPassword::class)->findOneByToken($token);

            if(!$reset_password){
                return $this->redirectToRoute('reset_password');
            }

        //vérifier le temps entre la demande et la validation -3 heurs
            $now = new DateTime();

            if($now > $reset_password->getCreatedAt()->modify('+ 3 hour') ){

             $this->addFlash('notice','Votre demande de mot de passe a expirée. Veuillez renouveller la demande');
            return $this->redirectToRoute('reset_password');
            }

        //retourner une vue pour modifier le mot de passe
        $form= $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()){

        $new_psw=$form->get('new_password')->getData(); 

        //encodage du mot de passe
        $password = $encoder->encodePassword($reset_password->getUser(),$new_psw);
        $reset_password->getUser()->setPassword($password);

        //flush en Bdd
        $this->entityManager->flush();

        //redirection vers la page login
        $this->addFlash('notice','Votre mot de passe a bien été mis à jour ');   
        return $this->redirectToRoute('app_login');

    }

    

    return $this->render('reset_password/update.html.twig',[
        'form'=>$form->createView()
    ]);

}
         
}