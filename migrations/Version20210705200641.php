<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210705200641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` ADD state INT NOT NULL, DROP isPaid, DROP is_paid');
        $this->addSql('ALTER TABLE product ADD is_best TINYINT(1) NOT NULL, DROP isBest');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` ADD isPaid TINYINT(1) DEFAULT \'0\' NOT NULL, ADD is_paid TINYINT(1) NOT NULL, DROP state');
        $this->addSql('ALTER TABLE product ADD isBest TINYINT(1) DEFAULT \'0\' NOT NULL, DROP is_best');
    }
}
